FROM node:latest

#
WORKDIR /app

# get sources
COPY . .

# install/build app
RUN npm install --production

# Container Execution
CMD ["node", "index.js"]